require 'rails_helper'

RSpec.describe ChangesController, type: :controller do
  describe 'GET #list' do
    it 'responds successfully with an HTTP 200 status code' do
      get :list, params: { from: Date.today.to_s, to: Date.today.to_s }, format: 'json'

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template(partial: '_list')
    end
  end
end
