require 'rails_helper'

RSpec.describe Change, type: :model do
  context 'Has required & valid fields' do
    it 'is invalid without a model' do
      change = Change.new
      expect(change).not_to be_valid
    end

    it 'is invalid without a model_id' do
      change = Change.new
      change.model = 'User'
      expect(change).not_to be_valid
    end

    it 'is invalid without a field' do
      change = Change.new
      change.model = 'User'
      change.model_id = 1
      expect(change).not_to be_valid
    end

    it 'is valid with model, model_id and field' do
      change = Change.new(model: 'User', model_id: 1, field: 'name', old: 'something')
      change_other = Change.new(model: 'User', model_id: 1, field: 'name', new: 'someother')

      expect(change).to be_valid
      expect(change_other).to be_valid
    end

    it 'is invalid if old and new are equal' do
      change = Change.new(model: 'User', model_id: 1, field: 'name', old: 'Hugo', new: 'Hugo')
      expect(change).not_to be_valid
    end

    it 'orders by last inserted' do
      hugo = Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo')
      bento = Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo Bento')

      expect(Change.first(2)).to eq([bento, hugo])
    end
  end

  context 'IN RANGE method gives responses in a range' do
    it 'in range should filter between dates' do
      Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo', created_at: Time.now.yesterday)
      bento = Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo Bento')

      change = Change.in_range(Date.today, Date.today)
      expect(change).to eq([bento])
    end

    it 'RESTRICT method replies only one record and one name change for the same user/range' do
      Change.create!(model: 'User', model_id: 1, field: 'name', new: 'Hugo')
      Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo', new: 'Hugo Bento')
      Change.create!(model: 'User', model_id: 1, field: 'name', old: 'Hugo Bento', new: 'Bento')

      change = Change.restrict(Date.today, Date.today)

      expect(change.length).to eq(1)
      expect(change.first[:old]).to eq(nil)
      expect(change.first[:new]).to eq('Bento')
    end
  end
end
