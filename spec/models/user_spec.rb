require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Has required & valid fields' do
    it 'is invalid without an entry' do
      user = User.new
      expect(user).not_to be_valid
    end
  end
end
