Rails.application.configure do
  config.sass.preferred_syntax = :scss

  config.assets.version = '1.0'
  # config.assets.precompile += ['wide/*', 'particular/*', 'models/*']

  config.assets.configure do |env|
    env.context_class.class_eval do
      include Rails.application.routes.url_helpers
    end
  end
end

# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += ['*']
