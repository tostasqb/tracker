class CreateChanges < ActiveRecord::Migration[5.0]
  def change
    create_table :changes do |t|
      t.string :model
      t.integer :model_id
      t.string :field
      t.string :old
      t.string :new

      t.timestamps
    end
  end
end
