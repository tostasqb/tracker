# Tracker

[![Build Status](https://travis-ci.org/tostasqb/tracker.svg?branch=master)](https://travis-ci.org/tostasqb/tracker)
[![Dependency Status](https://gemnasium.com/badges/github.com/tostasqb/tracker.svg)](https://gemnasium.com/github.com/tostasqb/tracker)
[![Code Climate](https://codeclimate.com/github/tostasqb/tracker/badges/gpa.svg)](https://codeclimate.com/github/tostasqb/tracker)
[![Test Coverage](https://codeclimate.com/github/tostasqb/tracker/badges/coverage.svg)](https://codeclimate.com/github/tostasqb/tracker/coverage)

Free interpretation of a User model tracker.

### Focus

**1st part**

- Receive 2 json objects (old & new)
- Track changes between both

**2nd part**

- Endpoint to deliver changes in each field over time
- Filter by date range

### Inputs

(On endpoint 1)

    // old
    {"_id": 1,
     "name": "Bruce Norries",
     "address": {"street": "Some street"}}

    // new
    {"_id": 1,
     "name": "Bruce Willis",
     "address": {"street": "Nakatomi Plaza"}}

### Outputs

(On endpoint 2)

    [{"field": "name", "old": "Bruce Norris", "new": "Bruce Willis"},
     {"field": "address.street", "old": "Some Street", "new": "Nakatomi Plaza"}]


## How this works

**Install**

- Download
- Bundle
- `rake db:migrate`
- `rails s`
- Open `http://localhost:3000` and test it out, submit one json and see the differences from previously selected one on the right, first submit won't give you feedback...
- An "_id" is required on the json object submitted.

**Code View**

- Most logic on controller
- Model `User` is being tracked
- Model `Change` is the tracker
- Frontend is the environment to add and track changes

## Out of scope

- No particular steps were made to deliver this as an API or service.
- At first sight my mind suggestions were to use [PaperTrail](https://github.com/airblade/paper_trail) or [ActiveModel's Dirty](http://api.rubyonrails.org/classes/ActiveModel/Dirty.html) but I guessed this wasn't the point of the exercise and I could also be in trouble since this takes on nested objects.

## Next steps

It would be natural that this would evolve to be a concern, a gem or a service.

## Versions

- Ruby 2.4.0
- Rails 5.0.1
- Sqlite3
