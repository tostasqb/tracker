class Change < ApplicationRecord
  validates :model, presence: true
  validates :model_id, presence: true
  validates_numericality_of :model_id, allow_nil: false
  validates :field, presence: true

  validate :validate_old_vs_new

  def validate_old_vs_new
    errors.add(:new, "can't be the same as old") if old == self[:new]
  end

  default_scope { order(id: :desc) }

  scope :latest, -> { limit(15) }

  def self.in_range(from, to)
    where('created_at >= ? and created_at <= ?', from.beginning_of_day, to.end_of_day)
  end

  def self.restrict(from, to)
    # clearer range
    f = from.beginning_of_day
    t = to.end_of_day

    sql = "SELECT
          	field,
          	model_id,

          	(select old from changes c
              where ch.model_id = c.model_id and ch.field = c.field and created_at >= ? and created_at <= ?
              order by id limit 1) as old,

            (select new from changes c2
              where ch.model_id = c2.model_id and ch.field = c2.field and created_at >= ? and created_at <= ?
              order by id desc limit 1) as new

          FROM changes ch
          WHERE created_at >= ? and created_at <= ?
          GROUP BY ch.field, ch.model_id"

    find_by_sql([sql, f, t, f, t, f, t])
  end
end
