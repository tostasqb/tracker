class UsersController < ApplicationController
  def index
    @user = User.new
  end

  def update
    # new
    new_entry = user_params
    new_json = JSON.parse(new_entry[:entry])
    new_entry[:user_id] = new_json['_id']

    # old
    old_entry = User.where(user_id: new_entry[:user_id]).last
    old_json = old_entry.nil? ? {} : JSON.parse(old_entry[:entry])

    # if json has differences, submit record
    if new_json != old_json
      rec = User.create(new_entry)

      # ...and submit differences
      discover_diffs(rec.user_id, old_json, new_json) unless old_json.empty?
    end

    render plain: 'ok'
  end

  private

  def discover_diffs(user_id, old_val, new_val, key = [])
    # differences by root keys
    diff = new_val.select { |k, v| v != old_val[k.to_s] }

    # discover difference in nested objects or submit
    diff.each do |k, v|
      key_dup = key.dup << k

      if v.is_a? Hash
        old_val = old_val[k]
        new_val = new_val[k]

        # It's a nested object! so re-discover!
        discover_diffs(user_id, old_val, new_val, key_dup)
      else
        submit_diffs(
          user_id,
          key_dup.join('.'), # keys separated by point (.)
          old_val[k],
          new_val[k]
        )
      end
    end
  end

  def submit_diffs(model_id, field, old_value, new_value)
    change = Change.new

    change.model = 'User'
    change.model_id = model_id
    change.field = field
    change.old = old_value
    change[:new] = new_value

    change.save!
  end

  def user_params
    params.require(:user).permit(:user_id, :entry)
  end
end
