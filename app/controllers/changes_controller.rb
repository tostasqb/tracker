class ChangesController < ApplicationController
  def list
    @changes =
      if params[:restrict] == 'true'
        Change.restrict(params[:from].to_time, params[:to].to_time)
      else
        Change.latest
              .in_range(params[:from].to_time, params[:to].to_time)
      end

    respond_to do |format|
      format.json { render partial: 'changes/list', format: :erb, layout: false }
    end
  end
end
